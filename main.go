package main

import (
	"fmt"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
)

func main() {
	// Initialize Echo
	e := echo.New()

	// Initialize other dependencies
	store := NewEventStore(logger)
	server := NewServer(store, logger)

	// Middleware
	e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogURI:    true,
		LogStatus: true,
		LogValuesFunc: func(c echo.Context, values middleware.RequestLoggerValues) error {
			logger.WithFields(logrus.Fields{
				"URI":    values.URI,
				"status": values.Status,
			}).Info("request")

			return nil
		},
	}))
	e.Use(middleware.Recover())

	// Routes
	e.POST("/api/games", server.RegisterGame)
	e.POST("/api/players", server.RegisterPlayer)
	e.POST("/api/games/:gameId/play", server.PlayRound)
	e.GET("/api/games/:gameId/state", server.GetGameState)
	e.PUT("/api/players", server.UpdatePlayer)
	e.DELETE("/api/players", server.DeletePlayer)
	e.PUT("/api/games", server.UpdateGame)
	// e.DELETE("/api/games", server.DeleteGame)

	// Start server
	fmt.Println("Starting server on port 8080")
	e.Start(":8080")
}

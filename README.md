---
title: XLM Arena
language_tabs:
  - shell: Shell
  - http: HTTP
  - javascript: JavaScript
  - ruby: Ruby
  - python: Python
  - php: PHP
  - java: Java
  - go: Go
toc_footers: []
includes: []
search: true
code_clipboard: true
highlight_theme: darkula
headingLevel: 2
generator: "@tarslib/widdershins v4.0.23"

---

# My Project

Base URLs:

* <a href="http://localhost:8080">localhost:8080: http://localhost:8080</a>

# Authentication

- HTTP Authentication, scheme: bearer

# xlm-arena

## POST /api/games

POST /api/games

> Body Parameters

```json
{
  "id": "game-1",
  "type": "example_game",
  "rules": {
    "number_of_rounds": 10,
    "round_time": 60,
    "objectives": [
      "objective1",
      "objective2"
    ],
    "win_conditions": [
      "condition1",
      "condition2"
    ]
  },
  "players": [],
  "weapons": [],
  "defenses": [],
  "state": "pending"
}
```

### Params

|Name|Location|Type|Required|Description|
|---|---|---|---|---|
|body|body|object| no |none|
|» id|body|string| yes |none|
|» type|body|string| yes |none|
|» rules|body|object| yes |none|
|»» number_of_rounds|body|integer| yes |none|
|»» round_time|body|integer| yes |none|
|»» objectives|body|[string]| yes |none|
|»» win_conditions|body|[string]| yes |none|
|» players|body|[string]| yes |none|
|» weapons|body|[string]| yes |none|
|» defenses|body|[string]| yes |none|
|» state|body|string| yes |none|

> Response Examples

> 201 Response

### Responses

|HTTP Status Code |Meaning|Description|Data schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|Inline|

### Responses Data Schema

## POST /api/players

POST /api/players

> Body Parameters

```json
{
  "id": "player-1",
  "name": "Player One",
  "health": 100
}
```

### Params

|Name|Location|Type|Required|Description|
|---|---|---|---|---|
|gameId|query|string| no |none|
|body|body|object| no |none|
|» id|body|string| yes |none|
|» name|body|string| yes |none|
|» health|body|integer| yes |none|

> Response Examples

> 201 Response

### Responses

|HTTP Status Code |Meaning|Description|Data schema|
|---|---|---|---|
|201|[Created](https://tools.ietf.org/html/rfc7231#section-6.3.2)|Created|Inline|
|403|[Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3)|Forbidden|Inline|

### Responses Data Schema

## POST /api/games/{gameId}/play

POST /api/games/{gameId}/play

### Params

|Name|Location|Type|Required|Description|
|---|---|---|---|---|
|gameId|path|string| yes |none|

> Response Examples

> 200 Response

```json
{}
```

### Responses

|HTTP Status Code |Meaning|Description|Data schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Success|Inline|

### Responses Data Schema

## GET /api/games/{gameId}/state

GET /api/games/{gameId}/state

### Params

|Name|Location|Type|Required|Description|
|---|---|---|---|---|
|gameId|path|string| yes |none|

> Response Examples

> 200 Response

```json
{}
```

### Responses

|HTTP Status Code |Meaning|Description|Data schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Success|Inline|

### Responses Data Schema

# Data Schema


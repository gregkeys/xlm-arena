package main

import (
	"encoding/json"
	"errors"
	"sort"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

type EventType string

type CRUDType string

const (
	CreateOperation CRUDType = "Create"
	UpdateOperation CRUDType = "Update"
	DeleteOperation CRUDType = "Delete"
)

type Event struct {
	ID        string    `json:"id"`
	Type      EventType `json:"type"`
	CRUD      CRUDType  `json:"crud"`
	Data      string    `json:"data"`
	Timestamp time.Time `json:"timestamp"`
}

type EventStore struct {
	mu     sync.Mutex
	events map[string][]Event
	logger *logrus.Logger
}

func NewEventStore(logger *logrus.Logger) *EventStore {
	return &EventStore{
		events: make(map[string][]Event),
		logger: logger,
	}
}

func (store *EventStore) Save(event Event) error {
	store.mu.Lock()
	defer store.mu.Unlock()
	store.logger.Info("Store Event:", event)

	// Check if the object has already been created
	if event.CRUD == CreateOperation {
		if len(store.events[event.ID]) > 0 {
			store.logger.Warn("Create operation denied: object already exists with ID:", event.ID)
			return errors.New("create operation denied: object already exists")
		}
	}

	// Check if the object has been deleted
	for _, e := range store.events[event.ID] {
		if e.CRUD == DeleteOperation {
			store.logger.Warn("Operation denied: object with ID has been deleted:", event.ID)
			return errors.New("operation denied: object has been deleted")
		}
	}

	store.events[event.ID] = append(store.events[event.ID], event)
	return nil
}

func (store *EventStore) GetAllEvents(eventID string) []Event {
	store.mu.Lock()
	defer store.mu.Unlock()

	events := store.events[eventID]

	// Sort events by Timestamp
	sort.Slice(events, func(i, j int) bool {
		return events[i].Timestamp.Before(events[j].Timestamp)
	})

	return events
}

// GetCurrentState processes all events and returns the current state of the object
func (store *EventStore) GetCurrentState(eventID string, result interface{}) error {
	store.mu.Lock()
	defer store.mu.Unlock()

	events, exists := store.events[eventID]
	if !exists {
		return errors.New("no events found for the given ID")
	}

	// Sort events by Timestamp
	sort.Slice(events, func(i, j int) bool {
		return events[i].Timestamp.Before(events[j].Timestamp)
	})

	state := make(map[string]interface{})

	for _, event := range events {
		var data map[string]interface{}
		if err := json.Unmarshal([]byte(event.Data), &data); err != nil {
			return err
		}

		switch event.CRUD {
		case CreateOperation, UpdateOperation:
			for key, value := range data {
				if value == nil {
					delete(state, key) // Remove field if value is nil
				} else {
					state[key] = value
				}
			}
		case DeleteOperation:
			state["state"] = "deleted"
		}
	}

	// Check if the object has been marked as deleted
	if state["state"] == "deleted" {
		return errors.New("object is deleted")
	}

	resultBytes, err := json.Marshal(state)
	if err != nil {
		return err
	}

	return json.Unmarshal(resultBytes, &result)
}

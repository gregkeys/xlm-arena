package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

const (
	GameRegisteredEventType   EventType = "GameRegistered"
	PlayerRegisteredEventType EventType = "PlayerRegistered"
	RoundPlayedEventType      EventType = "RoundPlayed"
)

type Server struct {
	store  *EventStore
	logger *logrus.Logger // Logger instance
}

func NewServer(store *EventStore, logger *logrus.Logger) *Server {
	return &Server{
		store:  store,
		logger: logger,
	}
}

func (s *Server) RegisterGame(c echo.Context) error {
	var game Game
	if err := c.Bind(&game); err != nil {
		c.Logger().Error("Failed to decode game:", err)
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}

	if s.isEventPresent(game.ID, GameRegisteredEventType, game.ID) {
		c.Logger().Info("Game ID already exists:", game.ID)
		return c.JSON(http.StatusConflict, map[string]string{"error": "game already exists"})
	}

	eventDataBytes, err := json.Marshal(game)
	if err != nil {
		c.Logger().Errorf("Failed to marshal event data: %v", err)
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "failed to register game"})
	}

	event := Event{
		ID:        game.ID,
		Type:      GameRegisteredEventType,
		CRUD:      CreateOperation,
		Data:      string(eventDataBytes),
		Timestamp: time.Now(),
	}
	err = s.store.Save(event)
	if err != nil {
		return c.JSON(http.StatusConflict, map[string]string{"error": err.Error()})
	}

	return c.NoContent(http.StatusCreated)
}

func (s *Server) UpdateGame(c echo.Context) error {
	var gameUpdate map[string]interface{}
	if err := c.Bind(&gameUpdate); err != nil {
		c.Logger().Error("Failed to decode game update:", err)
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}

	gameID := c.QueryParam("gameId")
	if gameID == "" {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "gameId is required"})
	}

	eventDataBytes, err := json.Marshal(gameUpdate)
	if err != nil {
		c.Logger().Errorf("Failed to marshal game update data: %v", err)
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "Failed to update game"})
	}

	event := Event{
		ID:        gameID,
		Type:      GameRegisteredEventType,
		CRUD:      UpdateOperation,
		Data:      string(eventDataBytes),
		Timestamp: time.Now(),
	}

	if err := s.store.Save(event); err != nil {
		return c.JSON(http.StatusConflict, map[string]string{"error": err.Error()})
	}

	return c.NoContent(http.StatusOK)
}

func (s *Server) DeleteGameField(c echo.Context) error {
	var fieldsToDelete map[string]interface{}
	if err := c.Bind(&fieldsToDelete); err != nil {
		c.Logger().Error("Failed to decode fields to delete:", err)
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}

	gameID := c.QueryParam("gameId")
	if gameID == "" {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "gameId is required"})
	}

	for key := range fieldsToDelete {
		fieldsToDelete[key] = nil // Indicate field deletion
	}

	eventDataBytes, err := json.Marshal(fieldsToDelete)
	if err != nil {
		c.Logger().Errorf("Failed to marshal fields to delete data: %v", err)
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "Failed to delete fields"})
	}

	event := Event{
		ID:        gameID,
		Type:      GameRegisteredEventType,
		CRUD:      UpdateOperation,
		Data:      string(eventDataBytes),
		Timestamp: time.Now(),
	}

	if err := s.store.Save(event); err != nil {
		return c.JSON(http.StatusConflict, map[string]string{"error": err.Error()})
	}

	return c.NoContent(http.StatusOK)
}

func (s *Server) RegisterPlayer(c echo.Context) error {
	c.Logger()
	var player Player
	if err := c.Bind(&player); err != nil {
		c.Logger().Error("Failed to decode player:", err)
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}

	gameId := c.QueryParam("gameId")
	if gameId == "" {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "gameId is required"})
	}

	if s.isEventPresent(gameId, PlayerRegisteredEventType, player.ID) {
		c.Logger().Infof("Player '%s' is already registered in game '%s'", player.ID, gameId)
		return c.JSON(http.StatusConflict, map[string]string{"error": "player already registered"})
	}

	eventDataBytes, err := json.Marshal(player)
	if err != nil {
		c.Logger().Errorf("Failed to marshal event data: %v", err)
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "failed to register player"})
	}

	event := Event{
		ID:        gameId, // Assuming gameId as event ID
		Type:      PlayerRegisteredEventType,
		CRUD:      UpdateOperation,
		Data:      string(eventDataBytes), // Store event data as string in Event
		Timestamp: time.Now(),
	}
	err = s.store.Save(event)
	if err != nil {
		return c.JSON(http.StatusConflict, map[string]string{"error": err.Error()})
	}

	return c.NoContent(http.StatusCreated)
}

func (s *Server) UpdatePlayer(c echo.Context) error {
	var player Player
	if err := c.Bind(&player); err != nil {
		c.Logger().Error("Failed to decode player:", err)
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}

	eventDataBytes, err := json.Marshal(player)
	if err != nil {
		c.Logger().Errorf("Failed to marshal event data: %v", err)
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "failed to update player"})
	}

	event := Event{
		ID:        player.ID,
		Type:      PlayerRegisteredEventType,
		CRUD:      UpdateOperation,
		Data:      string(eventDataBytes),
		Timestamp: time.Now(),
	}
	err = s.store.Save(event)
	if err != nil {
		return c.JSON(http.StatusConflict, map[string]string{"error": err.Error()})
	}

	return c.NoContent(http.StatusOK)
}

func (s *Server) DeletePlayer(c echo.Context) error {
	playerID := c.QueryParam("playerId")
	if playerID == "" {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "playerId is required"})
	}

	event := Event{
		ID:        playerID,
		Type:      PlayerRegisteredEventType,
		CRUD:      DeleteOperation,
		Data:      "",
		Timestamp: time.Now(),
	}
	err := s.store.Save(event)
	if err != nil {
		return c.JSON(http.StatusConflict, map[string]string{"error": err.Error()})
	}

	return c.NoContent(http.StatusOK)
}

func (s *Server) PlayRound(c echo.Context) error {
	// Implement your play round logic here
	return c.NoContent(http.StatusOK)
}

func (s *Server) GetGameState(c echo.Context) error {
	gameID := c.Param("gameId")
	if gameID == "" {
		c.Logger().Error("gameId is required")
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "gameId is required"})
	}

	events := s.store.GetAllEvents(gameID)
	game, err := BuildGame(events)
	if err != nil {
		c.Logger().Error("Failed to get game:", err)
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
	}
	return c.JSON(http.StatusOK, game)
}

func (s *Server) isEventPresent(eventId string, eventType EventType, dataID string) bool {
	// Query events based on eventId, eventType, etc. as needed
	events := s.store.GetAllEvents(eventId) // Example: Assuming eventId is used to filter events
	for _, event := range events {
		// Check if event matches the specified criteria
		if event.ID == eventId && event.Type == eventType {
			// Convert event.Data from string to []byte
			eventDataBytes := []byte(event.Data)

			// Implement logic to handle different event types and conditions
			switch eventType {
			case PlayerRegisteredEventType:
				// Unmarshal event data into a generic Player struct
				var eventData Player
				if err := json.Unmarshal(eventDataBytes, &eventData); err != nil {
					s.logger.Errorf("Failed to unmarshal event data: %v", err)
					continue
				}

				// Check if the player ID matches the dataID (player ID)
				if eventData.ID == dataID {
					s.logger.Infof("%s '%s' is already registered in event '%s'", eventType, dataID, eventId)
					return true
				}
				// Add more cases for other event types if needed
			}
		}
	}
	return false
}

// func marshal(v interface{}) string {
// 	data, _ := json.Marshal(v)
// 	return string(data)
// }

type Game struct {
	ID       string    `json:"id"`
	Type     string    `json:"type"`
	Rules    Rules     `json:"rules"`
	Players  []Player  `json:"players"`
	Weapons  []Weapon  `json:"weapons"`
	Defenses []Defense `json:"defenses"`
	State    string    `json:"state"`
}

type Rules struct {
	NumberOfRounds int      `json:"number_of_rounds"`
	RoundTime      int      `json:"round_time"`
	Objectives     []string `json:"objectives"`
	WinConditions  []string `json:"win_conditions"`
}

type Player struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Health int    `json:"health"`
}

type Weapon struct {
	Name   string `json:"name"`
	Damage int    `json:"damage"`
}

type Defense struct {
	Name  string `json:"name"`
	Armor int    `json:"armor"`
}

func ApplyEvent(game *Game, event Event) error {
	switch event.Type {
	case GameRegisteredEventType:
		var data Game
		if err := json.Unmarshal([]byte(event.Data), &data); err != nil {
			return err
		}
		*game = data
	case PlayerRegisteredEventType:
		var data Player
		if err := json.Unmarshal([]byte(event.Data), &data); err != nil {
			return err
		}
		game.Players = append(game.Players, data)
	case RoundPlayedEventType:
		// Handle round played event
	default:
		return errors.New("unknown event type")
	}
	return nil
}

func BuildGame(events []Event) (*Game, error) {
	game := &Game{}
	for _, event := range events {
		if err := ApplyEvent(game, event); err != nil {
			return nil, err
		}
	}
	return game, nil
}
